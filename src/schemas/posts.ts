import { Static, Type } from "@sinclair/typebox";

const PostSchema = Type.Object({
  _id: Type.String(),
  title: Type.String(),
  description: Type.String(),
  createdAt: Type.String(),
  isFavourite: Type.Boolean(),
});

const BodySchema = Type.Pick(PostSchema, ["title", "description"]);

const ParamsSchema = Type.Object({
  id: Type.String(),
});

export type Body = Static<typeof BodySchema>;
export type Params = Static<typeof ParamsSchema>;
export type Post = Static<typeof PostSchema>;

export const getPostsOptions = {
  schema: {
    response: {
      200: {
        type: "array",
        items: PostSchema,
      },
    },
  },
};

export const createPostOptions = {
  schema: {
    body: BodySchema,
    response: {
      201: PostSchema,
    },
  },
};

export const deletePostOptions = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },
};

export const updatePostOptions = {
  schema: {
    response: {
      200: PostSchema,
    },
  },
};

export const addToFavouritesOptions = {
  schema: {
    response: {
      200: PostSchema,
    },
  },
};
