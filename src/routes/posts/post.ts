import { FastifyPluginAsync } from "fastify";
import { basename } from "path";

import { Body, createPostOptions } from "../../schemas/posts";

const post: FastifyPluginAsync = async (fastify): Promise<void> => {
  const posts = fastify.posts();
  const httpMethod = fastify.requestMethod(basename(__filename));

  fastify.route<{ Body: Body }>({
    method: httpMethod,
    url: "/",
    schema: createPostOptions.schema,
    handler: async (request, reply) => {
      const { title, description } = request.body;

      const newPost = {
        title,
        description,
        createdAt: new Date().toISOString(),
        isFavourite: false,
      };

      await posts.insertOne(newPost);
      reply.code(201).send(newPost);
    },
  });
};

export default post;
