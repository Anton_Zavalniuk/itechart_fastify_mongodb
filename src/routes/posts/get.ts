import { FastifyPluginAsync } from "fastify";
import { basename } from "path";

import { getPostsOptions } from "../../schemas/posts";

const getAll: FastifyPluginAsync = async (fastify): Promise<void> => {
  const posts = fastify.posts();
  const httpMethod = fastify.requestMethod(basename(__filename));

  fastify.route({
    method: httpMethod,
    url: "/",
    schema: getPostsOptions.schema,
    handler: async function (request, reply) {
      const result = await posts?.find().toArray();
      reply.send(result);
    },
  });
};

export default getAll;
