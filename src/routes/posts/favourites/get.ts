import { FastifyPluginAsync } from "fastify";
import { basename } from "path";

import { getPostsOptions } from "../../../schemas/posts";

const getFavourites: FastifyPluginAsync = async (fastify): Promise<void> => {
  const posts = fastify.posts();
  const httpMethod = fastify.requestMethod(basename(__filename));

  fastify.route({
    method: httpMethod,
    url: "/",
    schema: getPostsOptions.schema,
    handler: async (request, reply) => {
      const result = await posts.find({ isFavourite: { $eq: true } }).toArray();
      reply.send(result);
    },
  });
};

export default getFavourites;
