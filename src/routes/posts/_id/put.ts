import { FastifyPluginAsync } from "fastify";
import { basename } from "path";

import { Body, Params, updatePostOptions } from "../../../schemas/posts";

const put: FastifyPluginAsync = async (fastify): Promise<void> => {
  const posts = fastify.posts();
  const objectId = fastify.objectId();
  const httpMethod = fastify.requestMethod(basename(__filename));

  fastify.route<{ Body: Body; Params: Params }>({
    method: httpMethod,
    url: "/",
    schema: updatePostOptions.schema,
    handler: async (request, reply) => {
      const { id } = request.params;
      const { title, description } = request.body;

      await posts.updateOne(
        { _id: objectId(id) },
        { $set: { title, description } }
      );

      const updatedPost = await posts.findOne({ _id: objectId(id) });
      reply.send(updatedPost);
    },
  });
};

export default put;
