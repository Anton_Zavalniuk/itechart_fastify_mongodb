import { FastifyPluginAsync } from "fastify";
import { basename } from "path";

import { deletePostOptions, Params } from "../../../schemas/posts";

const remove: FastifyPluginAsync = async (fastify): Promise<void> => {
  const posts = fastify.posts();
  const objectId = fastify.objectId();
  const httpMethod = fastify.requestMethod(basename(__filename));

  fastify.route<{ Params: Params }>({
    method: httpMethod,
    url: "/",
    schema: deletePostOptions.schema,
    handler: async (request, reply) => {
      const { id } = request.params;

      await posts.deleteOne({ _id: objectId(id) });
      reply.send({ message: `Item ${id} has been deleted` });
    },
  });
};

export default remove;
