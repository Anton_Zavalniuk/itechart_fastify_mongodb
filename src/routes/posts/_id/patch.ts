import { FastifyPluginAsync } from "fastify";
import { basename } from "path";

import { Params, addToFavouritesOptions, Post } from "../../../schemas/posts";

const patch: FastifyPluginAsync = async (fastify): Promise<void> => {
  const posts = fastify.posts();
  const objectId = fastify.objectId();
  const httpMethod = fastify.requestMethod(basename(__filename));

  fastify.route<{ Params: Params }>({
    method: httpMethod,
    url: "/",
    schema: addToFavouritesOptions.schema,
    handler: async (request, reply) => {
      const { id } = request.params;

      await posts
        .findOne({ _id: objectId(id) })
        .then((post: Post) =>
          posts.findOneAndUpdate(
            { _id: objectId(id) },
            { $set: { isFavourite: !post.isFavourite } }
          )
        );

      const updatedPost = await posts.findOne({ _id: objectId(id) });

      reply.send(updatedPost);
    },
  });
};

export default patch;
