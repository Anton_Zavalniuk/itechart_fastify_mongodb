import * as dotenv from "dotenv";
dotenv.config({ path: "../.env" });
import Fastify from "fastify";

import { app } from "./app";

const server = Fastify({
  logger: true,
});

server.register(app);

const start = async () => {
  try {
    await server.listen();
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};
start();
