import { HTTPMethods } from "fastify";
import fp from "fastify-plugin";

export default fp(async (fastify) => {
  const fileExtension = ".js";

  fastify.decorate("requestMethod", function (filename: string) {
    return filename
      .slice(0, filename.length - fileExtension.length)
      .toUpperCase() as HTTPMethods;
  });
});

declare module "fastify" {
  export interface FastifyInstance {
    requestMethod(filename: string): HTTPMethods;
  }
}
