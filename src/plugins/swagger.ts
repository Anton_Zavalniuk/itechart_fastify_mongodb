import fp from "fastify-plugin";
import swagger from "@fastify/swagger";
import swagger_ui from "@fastify/swagger-ui";

export default fp(async (fastify) => {
  fastify.register(swagger, {
    swagger: {
      info: {
        title: "Posts swagger",
        description: "Testing the Fastify swagger Posts API",
        version: "1.0.0",
      },
    },
  });

  fastify.register(swagger_ui, {
    routePrefix: "/docs",
    uiConfig: {
      docExpansion: "full",
      deepLinking: false,
    },
    uiHooks: {
      onRequest: function (request, reply, next) {
        next();
      },
      preHandler: function (request, reply, next) {
        next();
      },
    },
    staticCSP: true,
    transformStaticCSP: (header) => header,
    transformSpecification: (swaggerObject, request, reply) => {
      return swaggerObject;
    },
    transformSpecificationClone: true,
  });
});
