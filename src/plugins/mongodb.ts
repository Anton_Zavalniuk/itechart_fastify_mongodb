import fp from "fastify-plugin";
import mongodb from "@fastify/mongodb";

export default fp(async (fastify) => {
  await fastify.register(mongodb, {
    url: process.env.MONGODB,
  });

  fastify.decorate("posts", function () {
    return fastify?.mongo?.db?.collection("posts");
  });

  fastify.decorate("objectId", function () {
    return fastify.mongo.ObjectId;
  });
});

declare module "fastify" {
  export interface FastifyInstance {
    posts(): any;
    objectId(): (id: string) => string;
  }
}
